#include "bmp.h"

// Функция, подсчитывающая количество байтов для padding'а

static uint8_t get_padding(uint32_t width)
{
    return width % 4;
}

// Функция проверки BMP-заголовка на корректность

static enum read_status header_check(struct bmp_header *header)
{
    if (header->bfType != 0x4D42)
    {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24)
    {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

// Функция создания BMP-заголовка, соответствующего картинке

struct bmp_header image_header(const struct image *img)
{
    uint32_t width = (uint32_t)img->width;
    uint32_t height = (uint32_t)img->height;
    struct bmp_header res = {
        .bfType = 0x4D42,
        .bfileSize = (width * sizeof(struct pixel) + get_padding(width)) * height + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = (width * sizeof(struct pixel) + get_padding(width)) * height,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0};
    return res;
}

// Функция чтения из BMP-файла в структуру image, возвращающая в качестве результата статус чтения

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }
    if (header_check(&header))
    {
        return header_check(&header);
    }
    *img = create_image((uint64_t)header.biWidth, (uint64_t)header.biHeight);
    uint8_t padding = get_padding(header.biWidth);
    uint64_t counter = 0;
    for (uint64_t i = 0; i < img->height; i++)
    {
        for (uint64_t j = 0; j < img->width; j++)
        {
            if (fread(img->data + counter, sizeof(struct pixel), 1, in) != 1)
            {
                return READ_ERROR;
            }
            counter++;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

// Функция для вывода сообщения о результатах чтения в stderr

void read_status_message(enum read_status rs)
{
    if(rs == 0){
        fprintf(stdout, "%s", read_status_array[rs]);
    }else{fprintf(stderr, "%s", read_status_array[rs]);}
    
    if (rs != 0)
    {
        abort();
    }
}

// Функция записи изображения в BMP-файл, возвращающая в качестве результата статус записи

enum write_status to_bmp(FILE *out, struct image const *img)
{
    struct bmp_header header = image_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
    {
        return WRITE_ERROR;
    }
    uint8_t padding_byte = 0;
    uint32_t padding = get_padding(header.biWidth);
    uint64_t counter = 0;
    for (uint64_t i = 0; i < img->height; i++)
    {
        for (uint64_t j = 0; j < img->width; j++)
        {
            if (fwrite(img->data + counter, sizeof(struct pixel), 1, out) != 1)
            {
                return WRITE_ERROR;
            }
            counter++;
        }
        for (int i = 0; i < padding; i++)
        {
            if (fwrite(&padding_byte, sizeof(uint8_t), 1, out) != 1)
            {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

// Функция для вывода сообщения о результатах записи в stderr

void write_status_message(enum write_status ws)
{
    if(ws == 0){
        fprintf(stdout, "%s", write_status_array[ws]);
    }else{
        fprintf(stderr, "%s", write_status_array[ws]);
    }
    if (ws != 0)
    {
        abort();
    }
}
