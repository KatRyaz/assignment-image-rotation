#ifndef ROTATION_H
#define ROTATION_H
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image rotate_90degrees_ccw( struct image const source );

#endif //ROTATION_H
