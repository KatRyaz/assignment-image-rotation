#ifndef FILEHANDLER_H
#define FILEHANDLER_H
#include <stdio.h>
#include <stdlib.h>

// Перечисление возможных результатов открытия файла

enum open_status  {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum open_status f_open(char const *file_name, FILE** in, int mode);

// Массив сообщений о результатах открытия файла

static const char* open_status_array[] = {
    [OPEN_OK] = "File opened Successfully!\n",
    [OPEN_ERROR] = "Error occurred while opening file!\n"
};

void open_status_message(enum open_status os);

// Перечисление возможных результатов закрытия файла

enum  close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status f_close( FILE* out);

// Массив сообщений о результатах закрытия файла

static const char* close_status_array[] = {
    [CLOSE_OK] = "File closed Successfully!\n",
    [CLOSE_ERROR] = "Error occurred while closing file!\n"
};

void close_status_message(enum close_status cs);

#endif //FILEHANDLER_H
